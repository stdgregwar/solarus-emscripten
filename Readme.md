# Solarus on the web with emscripten

This repository contains a first working attempt to build solarus for the web.


## Checkout

Dont forget to clone using `--recursive` to pull the git submodules with the repo

## Add quest to embed

Add your `data.solarus` to `solarus/asset_dir`, this particular directory will be packaged in the produced js file.

## Compiling

To compile you need the emscripten SDK installed and correctly setup in your path.

Each dependency in the `external` folder must be compiled and installed to a
sysroot directory. The other dependencies are directly provided trough emscripten ports
and will be fetched and compile automagically by emsd

### GLM

Since glm is headers only we can directly reference the source folder

### Solarus

k

### Lua

```bash
emmake make ansi INSTALL_TOP=<path_to_sysroot> install   
```

### Physfs

```bash
mkdir build && cd build
emcmake cmake .. -DCMAKE_INSTALL_PREFIX=<path_to_sysroot>
emmake make install
```

### Modplug

```bash
mkdir build && cd build
emcmake cmake .. -DCMAKE_INSTALL_PREFIX=<path_to_sysroot>
emmake make install
```

### GLM

Since glm is headers only we can directly reference the source folder

### Solarus

```
mkdir build && cd build

emcmake cmake .. \
-DMODPLUG_INCLUDE_DIR=<path_to_sysroot>/include/libmodplug \
-DMODPLUG_LIBRARY=<path_to_sysroot>/lib/libmodplug.a \
-DPHYSFS_LIBRARY=<path_to_sysroot>/lib/libphysfs.a \
-DSOLARUS_USE_LUAJIT=Off \
-DLUA_LIBRARY=<path_to_sysroot>/lib/liblua.a \ 
-DLUA_INCLUDE_DIR=<path_to_sysroot>/include/ \ 
-DSOLARUS_GUI=Off \
-DSOLARUS_TESTS=Off \
-DGLM_INCLUDE_DIR=../../external/glm \
-DSOLARUS_BASE_WRITE_DIR="/persistent"

emmake make

cp ../../index.html .
```

## Testing

If you use some simple static html server to serve `solarus/build` folder. You should be able to boot the engine in a browser.
